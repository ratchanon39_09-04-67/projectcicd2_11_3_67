<?php
    $connect = mysqli_connect('localhost','root','Ms12345678','company');


    if (mysqli_connect_error($connect)){
        echo 'Failed to connect to database: ' . mysqli_connect_error();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP WITH MYSQL</title>
</head>
<body> 
    <table width="700" cellspacing="5" callspacding="5" border="1">
        <tr>
            <th>ID</th>
            <th>First_name</th>
            <th>Last_name</th>
            <th>Department</th>
            <th>Email</th>   
        </tr>
        <!-- $short hand for use direct html php -->
        <?php while($row = mysqli_fetch_array($result)): ?>
            <tr>
                <td><?php echo $row['id']?></td>
                <td><?php echo $row['first_name']?></td>
                <td><?php echo $row['last_name']?></td>
                <td><?php echo $row['department']?></td>
                <td><?php echo $row['email']?></td>
            </tr>
        <?php endwhile; ?>
        </table>
</body>
</html>